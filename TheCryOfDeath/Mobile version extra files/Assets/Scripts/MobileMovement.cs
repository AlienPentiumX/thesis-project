﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobileMovement : MonoBehaviour {

    Animator anim;
    private Rigidbody rb;
    public GameObject camera;
    public float speed;
    public Texture up,down,left,right,mid;
    public GameObject Dante;
    private bool movement;
    private PlayerShooting shoot;

    // Use this for initialization
    void Start () {
        anim = Dante.GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        shoot = Dante.GetComponent<PlayerShooting>();
    }

    private void Update()
    {
        if (isMoving() == false)
        {
            shoot.set_fire(true);
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
            if(shoot.get_fire())
                anim.Play("ShotgunAimIdle");
            else
                anim.Play("Start");
        }
    }

    void OnGUI () {
        movement = false;
        //kato
        if (GUI.RepeatButton(new Rect(Screen.width * (14 / 100f), Screen.height * (81 / 100f), Screen.width * (9 / 100f), Screen.height * (12 / 100f)), down))
        {
            shoot.set_fire(false);
            rb.velocity = camera.transform.forward * -15;
            rb.AddForce(-2 * camera.transform.forward * speed);
            anim.SetBool("back", true);
            anim.Play("Back");
            movement = true;
        }
        //pano
        if (GUI.RepeatButton(new Rect(Screen.width * (14 / 100f), Screen.height * (57 / 100f), Screen.width * (9 / 100f), Screen.height * (12 / 100f)), up))
        {
            shoot.set_fire(false);
            rb.velocity = camera.transform.forward * 15;
            rb.AddForce(2 * camera.transform.forward * speed);      
            anim.SetBool("isWalking", true);
            anim.Play("WalkFWD");
            movement = true;
        }

        // aristera
        if (GUI.RepeatButton(new Rect(Screen.width * (5 / 100f), Screen.height * (69 / 100f), Screen.width * (9 / 100f), Screen.height * (12 / 100f)), left))
        {
            shoot.set_fire(false);
            rb.velocity = camera.transform.right * -15;
            rb.AddForce(-2 * camera.transform.right * speed);
            anim.SetBool("Left", true);
            anim.Play("Left");
            movement = true;
        }
        //deksia
        if (GUI.RepeatButton(new Rect(Screen.width * (23 / 100f), Screen.height * (69 / 100f), Screen.width * (9 / 100f), Screen.height * (12 / 100f)), right))
        {
            shoot.set_fire(false);
            rb.velocity = camera.transform.right * 15;
            rb.AddForce(2 * camera.transform.right * speed);
            anim.SetBool("Right", true);
            anim.Play("Right");
            movement = true;
        }
        // treksimo
        if (GUI.RepeatButton(new Rect(Screen.width * (14 / 100f), Screen.height * (69 / 100f), Screen.width * (9 / 100f), Screen.height * (12 / 100f)), mid))
        {
            shoot.set_fire(false);
            rb.velocity = camera.transform.forward * 45;
            rb.AddForce(2 * camera.transform.forward * speed);
            anim.SetBool("isSprint", true);
            anim.Play("Sprint");
            movement = true;
        }

    }

    private bool isMoving()
    {
        return movement;
    }
}
